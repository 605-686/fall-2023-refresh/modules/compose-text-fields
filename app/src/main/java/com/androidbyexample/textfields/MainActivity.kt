package com.androidbyexample.textfields

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewModelScope
import com.androidbyexample.textfields.ui.theme.TextFieldsTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : ComponentActivity() {
    val viewModel by viewModels<PersonViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TextFieldsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Ui(viewModel)
//                    var name by remember { mutableStateOf("") }
//                    Column {
////                        BasicTextField(
////                            value = name,
////                            onValueChange = { name = it },
////                            modifier = Modifier
////                                .padding(8.dp)
////                                .fillMaxWidth(),
////                        )
////                        TextField(
////                            value = name,
////                            onValueChange = { name = it },
////                            label = { Text("First name")},
//////                            placeholder = { Text("Placeholder")},
////                            modifier = Modifier
////                                .padding(8.dp)
////                                .fillMaxWidth(),
////                        )
//                        OutlinedTextField(
//                            value = name,
//                            onValueChange = { name = it },
//                            label = { Text("Age")},
//                            keyboardOptions = KeyboardOptions(
//                                imeAction = ImeAction.Done,
//                                keyboardType = KeyboardType.Number
//                            ),
//                            keyboardActions = KeyboardActions(
//                                onSearch = {},
//                            ),
//                            modifier = Modifier
//                                .padding(8.dp)
//                                .fillMaxWidth(),
//                        )
//                    }
                }
            }
        }
    }
}

class MyViewModel: ViewModel() {
    val personFlow: MutableStateFlow<Person?> = MutableStateFlow(Person("p1", "", 0))
    fun updatePerson(person: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            delay(25)
            personFlow.value = person
        }
    }
}

data class Person(
    val id: String,
    val name: String,
    val age: Int,
)

@Composable
fun PersonEditScreen(
    person: Person?,
    onPersonChange: (Person) -> Unit,
) {
    person?.let {
        Column {
            OutlinedTextField(
                value = person.name,
                label = { Text("Name")},
                onValueChange = { onPersonChange(person.copy(name = it)) },
            )
            OutlinedTextField(
                value = person.age.toString(),
                label = { Text("Age")},
                onValueChange = { onPersonChange(person.copy(age = it.toInt())) },
                    // ignore that this can fail if the text isn't a number...
            )
        }
    } ?: Text(text  = "Loading")
}

class PersonRepository {
    fun update(person: Person) {
        personFlow.value = person
    }
    suspend fun getPersonById(id: String) = withContext(Dispatchers.IO) { personFlow.value }
    val personFlow = MutableStateFlow<Person?>(Person("p1", "Scott", 10))
}

@OptIn(FlowPreview::class)
class PersonViewModel(
): ViewModel() {
    private val personRepository: PersonRepository = PersonRepository()
    val personFlow = personRepository.personFlow
    suspend fun fetchPerson(id: String): Person? =
        personRepository.getPersonById(id)

    private var updateJob: Job? = null
    fun updatePerson(person: Person) {
        updateJob?.cancel()
        updateJob = viewModelScope.launch(Dispatchers.IO) {
            delay(500)
            personRepository.update(person)
            updateJob = null
        }
    }

    private val personUpdateFlow = MutableStateFlow<Person?>(null)
    init {
        viewModelScope.launch(Dispatchers.IO) {
            personUpdateFlow.collect { person ->
                person?.let { personRepository.update(it) }
            }
//            personUpdateFlow.debounce(500).collect { person ->
//                person?.let { personRepository.update(it) }
//            }
        }
    }
    fun updatePerson2(person: Person) {
        personUpdateFlow.value = person
    }
}

@Composable
fun PersonEditScreenAsync(
    id: String?,
    onPersonChange: (Person) -> Unit,
    fetchPerson: suspend (String) -> Person?,
) {
    var person by remember { mutableStateOf<Person?>(null) }
    LaunchedEffect(key1 = id) {
        person = id?.let { fetchPerson(it) }
    }

    person?.let { fetchedPerson ->
        Column {
            OutlinedTextField(
                value = fetchedPerson.name,
                label = { Text("Name")},
                onValueChange = {
                    val newPerson = fetchedPerson.copy(name = it)
                    person = newPerson
                    onPersonChange(newPerson)
                },
            )
            OutlinedTextField(
                value = fetchedPerson.age.toString(),
                label = { Text("Age")},
                onValueChange = {
                    val newPerson = fetchedPerson.copy(age = it.toInt())
                        // ignore that this can fail if the text isn't a number...
                    person = newPerson
                    onPersonChange(newPerson)
                },
            )
        }
    } ?: Text(text  = "Loading")
}

@Composable
fun PersonEditScreenSync(
    id: String?,
    onPersonChange: (Person) -> Unit,
    fetchPerson: suspend (String) -> Person?,
) {
    var person by remember { mutableStateOf<Person?>(null) }
    LaunchedEffect(key1 = id) {
        person = id?.let { fetchPerson(it) }
    }

    person?.let { fetchedPerson ->
        Column {
            OutlinedTextField(
                value = fetchedPerson.name,
                label = { Text("Name")},
                onValueChange = {
                    person = fetchedPerson.copy(name = it)
                },
            )
            OutlinedTextField(
                value = fetchedPerson.age.toString(),
                label = { Text("Age")},
                onValueChange = {
                    person = fetchedPerson.copy(age = it.toInt())
                        // ignore that this can fail if the text isn't a number...
                },
            )
            Button(
                onClick = {
                    person?.let { onPersonChange(it) }
                }
            ) {
                Text("Save")
            }
        }
    } ?: Text(text  = "Loading")
}

@Composable
fun Ui(
    viewModel: PersonViewModel,
) {
    val person by viewModel.personFlow.collectAsStateWithLifecycle(initialValue = null)
    Column {
        Text(text = "Person name in DB: ${person?.name}")
        PersonEditScreenSync(
            id = person?.id,
            onPersonChange = { editedPerson ->
                viewModel.updatePerson2(editedPerson)
            },
            fetchPerson = { viewModel.fetchPerson(it) }
        )
    }
}